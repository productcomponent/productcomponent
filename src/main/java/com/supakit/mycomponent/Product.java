/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supakit.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author SUPAKIT KONGKAM
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }

    public static ArrayList<Product> genProductList() {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "Espresso1", 40, "1.jfif"));
        list.add(new Product(2, "Espresso2", 30, "1.jfif"));
        list.add(new Product(3, "Americano1", 40, "3.jfif"));
        list.add(new Product(4, "Americano2", 30, "3.jfif"));
        list.add(new Product(5, "Americano3", 40, "3.jfif"));
        list.add(new Product(6, "Espresso3", 30, "1.jfif"));
        list.add(new Product(7, "Chocolate1", 40, "2.jfif"));
        list.add(new Product(8, "Chocolate2", 30, "2.jfif"));
        list.add(new Product(9, "Chocolate3", 40, "2.jfif"));
        list.add(new Product(10, "Espresso4", 30, "1.jfif"));
        return list;
    }
}
